clear

//========================================================================================================================//
// Setup
//========================================================================================================================//

local year $year
import delimited ./data/srvd_`year'.csv
label var start "Time of entry"
label var stop "Event/censoring time (days)"
label var status "Dead"
label var age "Age at entry"
label var sex "Sex"
label var type "Pop or COVID"
label define typelab 1 "`year'" 2 "COVID-19"
label values type typelab

gen male = sex == 2
gen covid = type==2

//remove immortal time
drop if start==0 & type==2

//stset with weights
stset stop [fweight=w], enter(start) f(status) 

//KM curve across pop and covid
// sts graph, 	by(type) ci ylabel(,format(%3.2f) angle(h)) 				///
// 			xtitle("Time since Feb 23rd (days)") ylabel(0.9(0.01)1) 	///
// 			legend(order(4 "COVID-19" 3 2 "`year'" 1)		 			///
// 			ring(0) pos(7) cols(2))
// graph export ./figures/KM_`year'_covid.pdf, replace



//========================================================================================================================//
// Model 1 - time since 1st Oct as main timescale for both cohorts
//========================================================================================================================//

gen malecovid = male * covid
gen age65covid = age65 * covid

//Royston-Parmar model

//select df for baseline
// forvalues i=1/9 {
// 	stpm2 if covid==0, df(`i') scale(h)
// 	est store m`i'
// }
// est stats m*

//select df for covid deviation from baseline
// forvalues i=1/4 {
// 	stpm2 covid, df(3) scale(h) dftvc(`i') tvc(covid)
// 	est store m`i'
// }
// est stats m*

//-> 3 splines for baseline 
//-> 3 splines for deviation from baseline (COVID)
//-> main effects for age and male + their interaction with covid
stpm2 age65 male age65covid malecovid covid, 	///
				df(3) scale(h) 					///
				tvc(covid) dftvc(3)

//timevar for predictions
range time 0 80 100
			
// //predict mortality rates for a male age 65
cap drop h*
predict h1, hazard at(covid 1 male 1 malecovid 1) zeros timevar(time) ci
predict h0, hazard at(male 1) zeros timevar(time) ci

twoway 	(rarea h0_lci h0_uci time)(line h0 time, sort) 								///
		(rarea h1_lci h1_uci time)(line h1 time, sort)								///
		, legend(order(2 "`year'" 1 "95% CI" 4 "COVID" 3 "95% CI"))					///
		/*ylabel(1e-5 0.0001 0.001 0.01 0.1, format(%6.5f) angle(h))*/	yscale(log)		///
		xtitle("Time since Feb 23rd (days)")										///
		title("Estimated mortality rates for a male aged 65")
// graph export ./figures/predicted_mort_rate_`year'_covid_male_age65.pdf, replace				

// //predict hazard ratio for covid, for a male age 65
cap drop hr*
predict hr, hrnum(covid 1 age65 0 male 1 age65covid 0 malecovid 1) 	///
			hrdenom(age65 0 male 1 age65covid 0 malecovid 0)		///
			timevar(time) ci

twoway 	(rarea hr_lci hr_uci time) (line hr time, sort) 					///
		, xtitle("Time since Feb 23rd (days)")								///
		title("Hazard ratio h(t | COVID-19)/h(t | `year'), male aged 65")	///
		legend(order(2 "Hazard ratio" 1 "95% CI")) 							///
		yscale(log) /*ylabel(0.5 1 2 5 10 20 50 100,  angle(h))*/
// graph export ./figures/predicted_mort_ratio_`year'_covid_male_age65.pdf, replace				




//========================================================================================================================//
// Model 2 - time since 1st Oct as main timescale for population
//         - time since diagnosis for COVID-19 cohort
//========================================================================================================================//

//fit separate models (but stacked to allow for simple CI calculations post-estimation)
//allowing for different timescales
merlin 	(_t age65 male if type==1, family(rp, df(3) failure(_d)) timevar(_t))													///
		(_t age65 male rcs(_t, df(3) orthog log event moffset(_t0)) if type==2, family(logchazard, failure(_d)) timevar(_t) )	///
		, weights(w) diff


//ratio of failure prob. at 60 days as a function of age, by sex
preserve

clear
set obs 60
range age 35 95
range age65 -30 34
expand 2
sort age65
bys age65: gen male = _n==1
label define malelab 0 "Female" 1 "Male"
label values male malelab

gen _t0 = 0
gen _t = 60
gen _d = 1
gen type = 1
gen w = 1

predict f0, cif outcome(1) causes(1) timevar(_t)

predict f1, cif outcome(2) causes(2) timevar(_t)

predictnl rm = log(predict(cif outcome(2) causes(2) timevar(_t))) - log(predict(cif outcome(1) causes(1) timevar(_t))) 	///
				, ci(rm_lci rm_uci)
replace rm = exp(rm)					
replace rm_lci = exp(rm_lci)
replace rm_uci = exp(rm_uci)
					
twoway 	(rarea rm_lci rm_uci age) (line rm age, sort) 		///
		, legend(order(2 "Ratio of failure" 1 "95% CI" ))			///
		xtitle("Age (years)") xlab(40(10)90)						///
		ytitle("Relative probability of death at 60 days")			///
		ylabel(0(10)30)												///
		by(male, note("") legend(off))
graph export ./figures/RM_60days_`year'.pdf, replace		

restore
