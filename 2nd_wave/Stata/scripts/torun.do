cd "/Users/Michael/Documents/covid_19_relative_mortality/RM_fall/stata"

//run analysis for specified years
forvalues year = 2014/2019 {
	global year `year'
	do ./scripts/analysis
}
