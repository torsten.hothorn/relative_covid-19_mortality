cd "/Users/Michael/Documents/relative_covid-19_mortality/Stata"

//run analysis for specified years
forvalues year = 2014/2018 {
	global year `year'
	do ./scripts/analysis
}
