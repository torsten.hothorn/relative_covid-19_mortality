load("~/Documents/covid_19_relative_mortality/manuscript/relative_mortality.rda")
setwd("~/Documents/covid_19_relative_mortality/parametric_models/data")

y<- as.matrix(srvd_2018$y)
data<-cbind(y,srvd_2018$age,srvd_2018$sex,srvd_2018$w,srvd_2018$age65,srvd_2018$type)
colnames(data) <- c("start","stop","status","age","sex","w","age65","type")
write.csv(data,file="srvd_2018.csv",row.names=F,na=".")

y<- as.matrix(srvd_2017$y)
data<-cbind(y,srvd_2017$age,srvd_2017$sex,srvd_2017$w,srvd_2017$age65,srvd_2017$type)
colnames(data) <- c("start","stop","status","age","sex","w","age65","type")
write.csv(data,file="srvd_2017.csv",row.names=F,na=".")

y<- as.matrix(srvd_2016$y)
data<-cbind(y,srvd_2016$age,srvd_2016$sex,srvd_2016$w,srvd_2016$age65,srvd_2016$type)
colnames(data) <- c("start","stop","status","age","sex","w","age65","type")
write.csv(data,file="srvd_2016.csv",row.names=F,na=".")

y<- as.matrix(srvd_2015$y)
data<-cbind(y,srvd_2015$age,srvd_2015$sex,srvd_2015$w,srvd_2015$age65,srvd_2015$type)
colnames(data) <- c("start","stop","status","age","sex","w","age65","type")
write.csv(data,file="srvd_2015.csv",row.names=F,na=".")

y<- as.matrix(srvd_2014$y)
data<-cbind(y,srvd_2014$age,srvd_2014$sex,srvd_2014$w,srvd_2014$age65,srvd_2014$type)
colnames(data) <- c("start","stop","status","age","sex","w","age65","type")
write.csv(data,file="srvd_2014.csv",row.names=F,na=".")