**Assessing Relative COVID-2019 Mortality: A Swiss Population-based Study**

[DOI: 10.1136/bmjopen-2020-042387](http://dx.doi.org/10.1136/bmjopen-2020-042387)

This repository contains R and Stata computer code to replicate results 
on relative COVID-19 mortality in Switzerland.

Top-level directory contains data and code for the first wave of spring
2020; the subdirectory 2nd_wave contains data and code for the 2nd wave of
fall 2020 ([DOI: 10.1136/bmjopen-2021-051164](http://dx.doi.org/10.1136/bmjopen-2021-051164)
).

*R*

* Make sure the following packages are installed:
  survival, memisc, tram, mlt, colorspace, lattice, latticeExtra,
  knitr, grid, gridExtra, mvtnorm, multcomp

* Run: R CMD BATCH Relative_Mortality.R

* Compare the output: diff Relative_Mortality.Rout Relative_Mortality.Rout.save

*Stata*

* Make sure the following packages are installed:
  stpm2, rcsgen, merlin

* Run: scripts/torun.do

* Compare to figures in figures/
